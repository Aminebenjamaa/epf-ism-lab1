# LAB 1 - node project on Heroku with Gitlab-CI

Small lab project for students to test how easy is to deploy and manage applications on PaaS. This will be also usefull to show them the difference between IaaS , PaaS and SaaS.

Good luck! 

 ## Tasks
 Remember, this code must be uploaded to your gitlab account(just follow the steps on your newly created app in gitlab).
 - update the `.gitlab-ci.yml` file and change the `$MY_APP_NAME` by your app name in heroku
 - update the `.gitlab-ci.yml` file and change the `$MY_API_KEY` by your api key in heroku
 - go to Gitlab under ci/cd and if there is something red call me :)
 - if everything is good you should be able to see your app running on Heroku. just click on `open app` in your heroku app
 - **Extra**: update your `index.js` and add something cool ( everything counts, surprise me ;D )

## Results
- Once you have a working app, you should be able to have a webpage with three inputs: Key, IV and Value.
- All you have to do its to write does strings into a PDF and add a print screen of the webpage (with the a visible link) and upload it to moodle ( TP 1)
- This is an individual work
